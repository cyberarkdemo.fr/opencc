# OpenCC - Connection components extraction made easy

## Prerequisites

- EPV Account with access to PVConfiguration.xml (PVWAConfig safe)
- Python 3 or Docker

## Using Docker (recommended)

```bash
# Create the output folder
$ mkdir dist

# Download PVConfiguration.xml
$ docker run -it --mount "type=bind,src=$PWD/dist,dst=/app/dist" registry.gitlab.com/cyberarkdemo.fr/opencc python download.py address auth_type username 
```
Add option `-k` or `--insecure` if your PVWA TLS certificate **is not signed by a public Certificate Authority**.

```bash
# Extract CCs from PVConfiguration.xml
$ docker run -it --mount "type=bind,src=$PWD/dist,dst=/app/dist" registry.gitlab.com/cyberarkdemo.fr/opencc python extract.py
```
Plugins are created in the `dist/` folder.

Alternatively you can build your own image:
```bash
# Download openCC sources
$ git clone https://gitlab.com/cyberarkdemo.fr/opencc.git
$ cd opencc

# Build docker image
$ docker build -t opencc .

# Download PVConfiguration.xml
$ docker run -it --mount "type=bind,src=$PWD/dist,dst=/app/dist" opencc python download.py address auth_type username 

# Extract CCs from PVConfiguration.xml
$ docker run -it --mount "type=bind,src=$PWD/dist,dst=/app/dist" opencc python extract.py
```

## Using Python

```bash
# Download openCC sources
$ git clone https://gitlab.com/cyberarkdemo.fr/opencc.git
$ cd opencc

# Download python dependencies
$ pip install -r requirements.txt

# Download PVConfiguration.xml
$ ./download.py [-h] [-p PASSWORD] [-s SAFE] [-f FILE] [-k | --insecure | --no-insecure] address type username

# Extract CCs from PVConfiguration.xml
$ ./extract.py [-h] [-f FILE] [-c CC]
```
Plugins are created in the `dist/` folder.

## TODO

- [X] Docker image 
- [X] Public release (v1)
- [ ] Better error handling

## Licensing

Copyright © 2021 CyberArk Software Ltd

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
