#!/usr/bin/env python
"""cyberarkdemo.fr - Vault File Downloader"""

import logging
import argparse
import getpass
import requests
import base64
import os

from bs4 import BeautifulSoup

try:
    from urllib import quote  # Python 2.X
except ImportError:
    from urllib.parse import quote  # Python 3+


def get_options():
    description = 'cyberarkdemo.fr - Vault File Downloader'
    parser = argparse.ArgumentParser(description=description)

    parser.add_argument('address', help='PVWA Address (https://<address>/PasswordVault)', type=str)
    parser.add_argument('type', help='Authentication Type', type=str)
    parser.add_argument('username', help='Username', type=str)
    parser.add_argument('-p', '--password', help='Password (prompted otherwise)', required=False, type=str)
    parser.add_argument('-s', '--safe', help='Safe (default: PVWAConfig)', required=False, type=str,
                        default="PVWAConfig")
    parser.add_argument('-f', '--file', help='File (default: PVConfiguration.xml)', required=False, type=str,
                        default="PVConfiguration.xml")
    parser.add_argument('-k', '--insecure', help='Disable TLS certificate verification', required=False,
                        type=bool, action=argparse.BooleanOptionalAction, default=False)
    args = parser.parse_args()

    # Ask password interactively
    if not args.password:
        args.password = getpass.getpass()

    return args


def set_logging(level=logging.INFO):
    new_logger = logging.getLogger()
    new_logger.setLevel(level)
    ch = logging.StreamHandler()
    new_logger.addHandler(ch)

    return new_logger


def download(address, type, username, password, safe, filename, verify=True):
    with requests.Session() as session:

        # Verify TLS certificate
        session.verify = verify

        # Mimic a browser with standard session headers
        session.headers.update({
            'Host': "{}".format(address),
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            'Accept-Encoding': 'gzip, deflate, br',
            'Connection': 'keep-alive',
            'Referer': "https://{}/PasswordVault/default.aspx".format(address),
            'Upgrade-Insecure-Requests': '1',
            'ReadyForDownload': 'true'
        })

        # Logging in - It's time for some cookies!
        login_info = {"username": username, "password": password, "type": type,
                      "secureMode": True,  "concurrentSession": True, "additionalInfo": ""}
        login_url = "https://{0}/PasswordVault/api/auth/{1}/logon".format(address, type)
        login_request = session.post(login_url, data=login_info)

        if login_request.status_code != requests.codes.ok:
            exit("Authentication failure")

        # First download request - It checks that we're authenticated and gives us values to send in a form
        download_parameters = "{0}^@^Root^@^{1}^@^0^@^False^@^False^@^^@^BackURL=MsgErr=MsgInfo=7".format(safe,
                                                                                                          filename)
        payload = quote(base64.b64encode(download_parameters.encode("utf-8")))
        download_url_1 = "https://{0}/PasswordVault/downloadfile.aspx?Data={1}".format(address, payload)

        download_request_1 = session.get(download_url_1)
        if download_request_1.status_code != requests.codes.ok:
            exit("Fail to locate PVConfiguration.xml")

        # Second download request - Fill the form with the generated values
        html = BeautifulSoup(download_request_1.text, 'html.parser')
        viewstate = html.find(id='__VIEWSTATE')
        viewstate_generator = html.find(id='__VIEWSTATEGENERATOR')
        viewstate_encrypted = html.find(id='__VIEWSTATEENCRYPTED')
        download_form_info = {
            '__VIEWSTATE': viewstate.get('value'),
            '__VIEWSTATEGENERATOR': viewstate_generator.get('value'),
            '__VIEWSTATEENCRYPTED': viewstate_encrypted.get('value'),
            'hiddenTimeZone': '0'  # Why?!
        }

        download_url_2 = "https://{0}/PasswordVault/default.aspx".format(address)
        download_request_2 = session.post(download_url_2, data=download_form_info)
        if download_request_2.status_code != requests.codes.ok:
            exit("Fail to fetch PVConfiguration.xml")

        logger.info("{} downloaded successfully".format(filename))

        return download_request_2.text


if __name__ == "__main__":
    options = get_options()
    logger = set_logging()

    file = download(options.address, options.type, options.username, options.password, options.safe, options.file,
                    verify=not options.insecure)

    output_dirname = "dist"
    if not os.path.exists(output_dirname):
        os.makedirs(output_dirname)

    # Write output to file
    with open(os.path.join(output_dirname, options.file), 'w') as f:
        f.write(file)
    f.close()
