FROM python:alpine

ENV PYTHONWARNINGS="ignore:Unverified HTTPS request"
ENV USER=docker
ENV UID=1000

WORKDIR /app

# Set up our runtime user.
RUN adduser \
    --disabled-password \
    --gecos "" \
    --home "$(pwd)" \
    --no-create-home \
    --uid "$UID" \
    "$USER"

RUN chown "$UID:$UID" "$PWD"

USER "$USER"

# Copy project and install dependencies
COPY requirements.txt requirements.txt

RUN pip3 install --no-warn-script-location -r requirements.txt

COPY . .