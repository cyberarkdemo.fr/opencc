#!/usr/bin/env python
"""cyberarkdemo.fr - Connection Component Exporter"""

import logging
import argparse
import os

import xml.etree.ElementTree as ET
import xml.dom.minidom

import shutil


def get_options():
    description = 'cyberarkdemo.fr - Connection Component Extractor'
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('-f', '--file', help='File (default: PVConfiguration.xml)', required=False,
                        type=str,
                        default="PVConfiguration.xml")
    parser.add_argument('-c', '--cc', help='Connection Component', required=False, type=str)
    args = parser.parse_args()

    return args


def set_logging(level=logging.INFO):
    new_logger = logging.getLogger()
    new_logger.setLevel(level)
    ch = logging.StreamHandler()
    new_logger.addHandler(ch)

    return new_logger


def load_pvconfig(filename):
    try:
        tree = ET.ElementTree()
        pvconfig_root = tree.parse(filename)
        return pvconfig_root
    except IOError as e:
        print("{} is missing! Aborting...".format(options.file))
        exit(-1)

    return None


def create_folder(dirname):
    # Create folder to store exported connection components
    if not os.path.exists(dirname):
        os.mkdir(dirname)
        logger.debug("Folder {} has been created.".format(dirname))

    return dirname


def get_cc_name(cc):
    if type(cc) == str:
        cc_name = cc
    else:
        # Use cc-id "PSM-XXXX" to generate cc-filename "CC-XXXX"
        cc_name = cc.attrib['Id']
        logger.debug("CC ID is {}".format(cc_name))

    # Remove PSM prefix
    if cc_name.startswith('PSM-'):
        cc_name = cc_name[4:]
    else:
        cc_name = cc_name
    cc_name = "CC-{}".format(cc_name)
    logger.debug("CC File ID is {}".format(cc_name))

    return cc_name


def extract_cc(cc, output_dirname):
    cc_name = get_cc_name(cc)

    # Create folder for current connection component
    cc_folder = os.path.join(output_dirname, cc_name)
    create_folder(cc_folder)

    # Export connection component to xml file
    pretty_print_xml(cc, os.path.join(cc_folder, cc_name + '.xml'))
    logger.info("Exporting {}...".format(cc_name))

    # Create zip archive
    # To understand make_archive args see
    # https://docs.python.org/dev/library/shutil.html#archiving-example-with-base-dir
    shutil.make_archive(cc_folder, 'zip', output_dirname, cc_name)


# Source: https://stackoverflow.com/questions/749796/pretty-printing-xml-in-python
# Source: https://stackoverflow.com/questions/2933262/how-to-write-an-xml-file-without-header-in-python/11355395
def pretty_print_xml(root, output_file):
    """
    Useful for when you are editing xml data on the fly
    """
    xml_string = xml.dom.minidom.parseString(ET.tostring(root))
    xml_string = xml_string.childNodes[0]  # Remove <?xml> header
    xml_string = xml_string.toprettyxml(indent="  ")  # Prettify with 2 spaces
    xml_string = os.linesep.join(
        [s for s in xml_string.splitlines() if s.strip()])  # Remove the weird newline issue
    with open(output_file, "w") as file_out:
        file_out.write(xml_string)


if __name__ == "__main__":
    options = get_options()
    logger = set_logging()

    output_dirname = "dist"
    cc_output_dirname = "connection_components"

    cc_output_full_dirname = os.path.join(output_dirname, cc_output_dirname)
    pvconfig = load_pvconfig(os.path.join(output_dirname, options.file))

    if pvconfig:
        create_folder(cc_output_full_dirname)
        if options.cc:
            # Extract only cc in parameter
            for cc in pvconfig.findall(".//ConnectionComponent"):
                if get_cc_name(cc) == get_cc_name(options.cc):
                    extract_cc(cc, cc_output_full_dirname)
        else:
            # Extract All
            logging.info("Extract ALL")
            for cc in pvconfig.findall(".//ConnectionComponent"):
                extract_cc(cc, cc_output_full_dirname)
